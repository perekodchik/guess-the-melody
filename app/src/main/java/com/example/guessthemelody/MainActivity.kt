package com.example.guessthemelody

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun goToIntent(view: View) {
        val button = view as Button
        val genreInt = when(button.text) {
            getString(R.string.pop)     -> 1
            getString(R.string.anime)   -> 2
            getString(R.string.rock)    -> 3
            getString(R.string.classic) -> 4
            else                        -> 0
        }

        val melodyIntent = Intent(this, MelodyActivity::class.java)
        melodyIntent.putExtra(MelodyActivity.GENRE, genreInt)
        startActivity(melodyIntent)

    }
}