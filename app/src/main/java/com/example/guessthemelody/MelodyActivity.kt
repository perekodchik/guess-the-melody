package com.example.guessthemelody

import android.annotation.SuppressLint
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_melody.*
import java.util.ArrayList

class MelodyActivity : AppCompatActivity() {

    companion object {
        const val GENRE = "genre"
    }

    private var mediaPlayer: MediaPlayer? = null
    private var genre: Int = 0
    private var rightGuessesCounter = 0
    private var rightGuesses : TextView? = null
    private var actualSongSource = 0

    /*  Simple enum
        0 - Everything
        1 - Pop
        2 - Anime
        3 - Rock
        4 - Classical */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_melody)

        button1 = findViewById<Button>(R.id.button1)
        button2 = findViewById<Button>(R.id.button2)
        button3 = findViewById<Button>(R.id.button3)
        button4 = findViewById<Button>(R.id.button4)
        nextSongButton = findViewById<Button>(R.id.nextSong)
        rightGuesses = findViewById(R.id.rightGuesses)
        notify = findViewById(R.id.notification)

        everythingMelodies.addAll(popMelodies)
        everythingMelodies.addAll(animeMelodies)
        everythingMelodies.addAll(rockMelodies)
        everythingMelodies.addAll(classicMelodies)
        fakeEverythingNames.addAll(fakePopNames)
        fakeEverythingNames.addAll(fakeAnimeNames)
        fakeEverythingNames.addAll(fakeRockNames)
        fakeEverythingNames.addAll(fakeClassicNames)

        genre = intent.getIntExtra(GENRE, 0)
        val selectedGenre : TextView = findViewById(R.id.selectedGenre)
        val genreStr = when(genre) {
            1 -> "Pop"
            2 -> "Anime"
            3 -> "Rock"
            4 -> "Classical"
            else -> "Everything" }
        selectedGenre.text = getString(R.string.selected_genre_s).format(genreStr)


        rightGuesses?.text = getString(R.string.right_guesses_d).format(rightGuessesCounter)

        requestSong()

    }

    override fun onPause() {
        super.onPause()
        mediaPlayer!!.stop()
    }

    override fun onResume() {
        super.onResume()
        mediaPlayer!!.start()
    }

    override fun onRestart() {
        super.onRestart()
        mediaPlayer!!.start()
    }

    fun replaySong(view: View) {
        mediaPlayer?.stop()
        mediaPlayer = MediaPlayer.create(this, actualSongSource)
        mediaPlayer!!.start()
    }

    @SuppressLint("SetTextI18n")
    fun buttonClick(view: View) {
        val button = view as Button

        nextSongButton?.visibility = Button.VISIBLE
        button1?.visibility = Button.INVISIBLE
        button2?.visibility = Button.INVISIBLE
        button3?.visibility = Button.INVISIBLE
        button4?.visibility = Button.INVISIBLE

        if(button.text == actualSongName){
            rightGuessesCounter++
            rightGuesses?.text = getString(R.string.right_guesses_d).format(rightGuessesCounter)
            notify?.text = "You guessed right!"
        }
        else {
            notify?.text = "You guessed wrong. The song name was " + actualSongName
        }
    }

    private fun requestSong() {

        nextSongButton?.visibility = Button.INVISIBLE
        button1?.visibility = Button.VISIBLE
        button2?.visibility = Button.VISIBLE
        button3?.visibility = Button.VISIBLE
        button4?.visibility = Button.VISIBLE
        notify?.text = ""

        var songNames = mutableListOf<String>()
        val fakeNames = mutableListOf<String>()
        mediaPlayer?.stop()
        when(genre){
            1 -> {
                popMelodies.shuffle()
                fakeNames.addAll(fakePopNames)
                fakeNames.addAll(getSongNames(popMelodies.subList(1,popMelodies.size-1)))
                songNames.add(popMelodies[0].songName)
                actualSongSource = popMelodies[0].source
                mediaPlayer = MediaPlayer.create(
                    this, popMelodies[0].source
                )
            }
            2 -> {
                animeMelodies.shuffle()
                fakeNames.addAll(fakeAnimeNames)
                fakeNames.addAll(getSongNames(animeMelodies.subList(1,animeMelodies.size-1)))
                songNames.add(animeMelodies[0].songName)
                actualSongSource = animeMelodies[0].source
            }
            3 -> {
                rockMelodies.shuffle()
                fakeNames.addAll(fakeRockNames)
                fakeNames.addAll(getSongNames(rockMelodies.subList(1,rockMelodies.size-1)))
                songNames.add(rockMelodies[0].songName)
                actualSongSource = rockMelodies[0].source
            }
            4 -> {
                classicMelodies.shuffle()
                fakeNames.addAll(fakeClassicNames)
                fakeNames.addAll(getSongNames(classicMelodies.subList(1,classicMelodies.size-1)))
                songNames.add(classicMelodies[0].songName)
                actualSongSource = classicMelodies[0].source
            }
            else -> {
                everythingMelodies.shuffle()
                fakeNames.addAll(fakeEverythingNames)
                fakeNames.addAll(getSongNames(everythingMelodies.subList(1,everythingMelodies.size-1)))
                songNames.add(everythingMelodies[0].songName)
                actualSongSource = everythingMelodies[0].source
            }
        }
        mediaPlayer = MediaPlayer.create(this, actualSongSource)
        actualSongName = songNames[0]
        fakeNames.shuffle()
        songNames.add(fakeNames[0])
        songNames.add(fakeNames[1])
        songNames.add(fakeNames[2])
        songNames.shuffle()



        button1?.text = songNames[0]
        button2?.text = songNames[1]
        button3?.text = songNames[2]
        button4?.text = songNames[3]

        mediaPlayer!!.start()
    }

    fun requestSongClick(view: View){
        requestSong()
    }

    var button1: Button? = null
    var button2: Button? = null
    var button3: Button? = null
    var button4: Button? = null
    var nextSongButton: Button? = null
    var notify: TextView? = null


    private val popMelodies = mutableListOf<Melody>(
        Melody(R.raw.blank_space, "Taylor Swift - Blank Space"),
        Melody(R.raw.imagine_dragons_demons, "Imagine Dragons - Demons"),
        Melody(R.raw.weeknd_blinding_lights, "The Weeknd - Blinding Lights"),
        Melody(R.raw.bts_dynamite, "BTS - Dynamite"),
        Melody(R.raw.taylor_bad_blood, "Taylor Swift - Bad Blood"),
        Melody(R.raw.brake_fake_love, "BTS - Fake Love"),
        Melody(R.raw.taylor_cold_as_you, "Taylor Swift - Cold As You"),
        Melody(R.raw.carly_rae_jepsen_call, "Carly Rae Jepsen - Call Me Maybe"),
        Melody(R.raw.taylor_trouble, "Taylor Swift - I Knew You Were Trouble")
    )

    private val fakePopNames = mutableListOf<String>(
        "Lana Del Rey - Summertime Sadness",
        "Lana Del Rey - Change",
        "Lana Del Rey - Cruel World",
        "Miley Cyrus - Wrecking Ball",
        "Miley Cyrus - The Most",
        "Miley Cyrus - The Bitch Is Back",
        "Taylor Swift - The Bitch Is Back",
        "Taylor Swift - The Most",
        "Taylor Swift - Forget It",
        "BTS - Follow Me",
        "BTS - Fireworks",
        "BTS - Trouble"
    )

    private val animeMelodies = mutableListOf<Melody>(
        Melody(R.raw.naruto_silhouette, "Naruto - Silhouette"),
        Melody(R.raw.ghoul_unravel, "Tokyo Ghoul - Unravel"),
        Melody(R.raw.jojo_giorno, "JoJo - Giorno's Theme"),
        Melody(R.raw.sao_crossing_field, "SAO - Crossing Field"),
        Melody(R.raw.naruto_shalala, "Naruto - Sha La La"),
        Melody(R.raw.naruto_blue_bird, "Naruto - Blue Bird"),
        Melody(R.raw.naruto_sadness, "Naruto - Sadness And Sorrow"),
        Melody(R.raw.vinland_opening, "The Vinland Saga- Opening 1"),
        Melody(R.raw.nogame_opening, "No Game No Life - Opening"),
        Melody(R.raw.kon_listen, "K-On - Listen"),
        Melody(R.raw.deathnote_ltheme, "Death Note - L's Theme"),
        Melody(R.raw.alchemist_again, "Fullmetal Alchemist - Again"),
        Melody(R.raw.jojo_town, "JoJo - Crazy Noisy Bizarre Town")
    )
    private val fakeAnimeNames = mutableListOf<String>(
        "JoJo - Sono Chi No Sadame",
        "Naruto - Friends",
        "Naruto - Come With Me",
        "Spirited Away - Intro",
        "Your Name - Meteor",
        "Konosuba - Opening 1",
        "Kaguya-sama - Miyuki's Theme",
        "Kaguya-sama - Chika Dance"
    )

    private val rockMelodies = mutableListOf<Melody>(
        Melody(R.raw.muse_uprising, "Muse - Uprising"),
        Melody(R.raw.deep_smoke, "Deep Purple - Smoke On The Water"),
        Melody(R.raw.metallica_nothing, "Metallica - Nothing Else Matters"),
        Melody(R.raw.linkin_numb, "Linking Park - Numb"),
        Melody(R.raw.sabaton_resit_and_bite, "Sabaton - Resist And Bite"),
        Melody(R.raw.golden_earring_going_to_the_run, "Golden Earring - Going To The Run")
    )

    private val fakeRockNames = mutableListOf<String>(
        "Muse - Till Dawn",
        "Metallica - Whiskey In The Jar",
        "Red Hot Chili Peppers - Otherside",
        "Queen - Under Pressure",
        "Metallica - Whiskey In The Jar",
        "Ramstein - Mein Herz Brennt",
        "Queen - Bohemian Phapsody",
        "Linking Park - In The End"
    )

    private val classicMelodies = mutableListOf<Melody>(
        Melody(R.raw.beethoven_elise, "Beethoven - Fur Elise"),
        Melody(R.raw.beethoven_moonlight, "Beethoven - Moonlight Sonata"),
        Melody(R.raw.liszt_campanella, "Liszt - La Campanella"),
        Melody(R.raw.chopin_nocturne, "Chopin - Nocturne"),
        Melody(R.raw.vivaldi_spring, "Vivaldi - Spring"),
        Melody(R.raw.bach_toccata, "Bach - Toccata And Fugue"),
        Melody(R.raw.rimsky_bumblebee, "Rimsky-Korsakov - Flight Of The Bumbleebee"),
        Melody(R.raw.mozart_turkish, "Mozart - Turkish March"),
        Melody(R.raw.bach_minuet, "Bach - Minuet in G Major"),
        Melody(R.raw.chopin_fantasie_impromptu, "Chopin - Fantasie Impromptu")
    )

    private val fakeClassicNames = mutableListOf<String>(
        "Beethoven - Etude no. 9",
        "Liszt - Sonata in C# minor",
        "Chopin - Hungarian Rhapsody",
        "Mozart - La Camplanella",
        "Rachmaninoff - Prelude no. 2",
        "Beethoven - Ode To Joy",
        "Strauss - Etude no. 3",
        "Chopin - op. 67 in F minor",
        "Mozart - Time in Eb Major"
    )

    private val everythingMelodies = mutableListOf<Melody>()
    private val fakeEverythingNames = mutableListOf<String>()

    fun  getSongNames(list: MutableList<Melody>): MutableList<String> {
        var newList = mutableListOf<String>()
        for(song in list) {
            newList.add(song.songName)
        }
        return newList
    }

    private var actualSongName = ""

}